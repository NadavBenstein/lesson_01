#include "linkedList.h"
#include <iostream>
using namespace std;


listNode* createNode(int num)
{
	listNode* node = new listNode;
	node->num = num;
	node->next = NULL;

	return node;
}

listNode* addNode(listNode* head, listNode* addThis)
{
	addThis->next = head;
	head = addThis;
	return head;
}

listNode* removeNode(listNode* head)
{
	if (head->next != NULL)
	{
		head = head->next;
	}
	
	return head;
}

void printList(listNode* head)
{
	listNode* curr = head;
	while (curr)
	{
		cout << curr->num << "\n";
		curr = curr->next;
	}
	cout << "\n";
}