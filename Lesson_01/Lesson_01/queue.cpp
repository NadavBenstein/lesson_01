#include "queue.h"
#include <iostream>
using namespace std;

void initQueue(queue* q, unsigned int size)
{
	q->elements = new int[size];
	q->size = size;
	q->rear = -1;
	q->front = -1;
}

int dequeue(queue* q)
{
	int result = 0;

	if (isEmpty(q))
	{
		cout << "Queue is Empty\n";
		result = -1;
	}
	else
	{
		if (q->front == q->rear)
		{
			q->front = -1;
			q->rear = -1;
		}
		else
		{
			q->front++;
		}
		result = q->elements[q->front];
	}
	return result;
}

void cleanQueue(queue* q)
{
	delete[] q->elements;
	q->rear = -1;
	q->front = -1;
}

void enqueue(queue* q, unsigned int newValue)
{
	if (q->rear == q->size - 1)
	{
		cout << "the queue is full\n";
	}
	else
	{
		if (q->front == -1)
		{
			q->front = 0;
		}
		q->rear++;
		q->elements[q->rear] = newValue;
	}
}

void showFront(queue* q)
{
	if (isEmpty(q))
	{
		cout << "queue is empty\n";
	}
	else
	{
		cout << "the element at the front is: " << q->elements[q->front] << "\n";
	}
}

void displayQueue(queue* q)
{
	int i = q->front;

	if (isEmpty(q))
	{
		cout << "the queue is empty\n";
	}
	else
	{
		for (i; i <= q->rear; i++)
		{
			cout << q->elements[i] << "\n";
		}
	}
}

bool isEmpty(queue* q) 
{
	bool result = false;
	if (q->rear == -1 && q->front == -1)
	{
		result = true;
	}

	return result;
}

