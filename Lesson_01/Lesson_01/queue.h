#ifndef QUEUE_H
#define QUEUE_H


/* a queue contains positive integer values. */
typedef struct queue
{
	int* elements;
	int size;
	int front;
	int rear;
} queue;

bool isEmpty(queue* q);
void initQueue(queue* q, unsigned int size);
void cleanQueue(queue* q);

void enqueue(queue* q, unsigned int newValue);
int dequeue(queue* q); // return element in top of queue, or -1 if empty

void showFront(queue* q);
void displayQueue(queue* q);

#endif /* QUEUE_H */